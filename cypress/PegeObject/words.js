class Words{

  //Создание рандомного предложения
  wordsRU = ["поручение", "проект", "тест", "задача", "проверка", "распоряжение", "сроки", "входящие", "заявка",
  "договор", "исполнить", "основание", "резолюция", "факт", "департамент", "пользователь", "внутренний"];
  wordsEN = ["instruction", "project", "test", "task", "check", "order", "deadlines", "incoming", "application",
    "contract", "execute", "ground", "resolution", "fact", "department", "user", "internal"]
  
  getRandomWordRU(firstLetterToUppercase = false) {
    const word = this.wordsRU[this.randomNumber(0, this.wordsRU.length - 1)];
    return firstLetterToUppercase ? word.charAt(0).toUpperCase() + word.slice(1) : word;
  }
  
  generateWords(length = 17) {
    let arr = []
    for (let i = 0; i < length; i++) {
      arr.push(this.getRandomWord(i === 0))
    }
    return arr.join(' ').trim() + '.';
  }
      
  randomNumber(min, max) {
    return Math.round(Math.random() * (max - min) + min);
  }
  

  getRandomWordEN(firstLetterToUppercase = false) {
    const word = this.wordsEN[this.randomNumber(0, this.wordsEN.length - 1)];
    return firstLetterToUppercase ? word.charAt(0).toUpperCase() + word.slice(1) : word;
  };
}

export default Words