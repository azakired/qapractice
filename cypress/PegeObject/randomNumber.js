class RandomNumber {

  randomNumb(length) {
    let randomNumber = "";
    var pattern = "123456789";
    for (var i = 0; i < length; i++) {
      randomNumber += pattern.charAt(Math.floor(Math.random() * pattern.length));
    }   
    return randomNumber;
  };

  randomNumbIn(min, max) {
    let randomNumber = "";       
    randomNumber = Math.floor(Math.random() * (max - min) + min); //max не включает значение
    return randomNumber;
  };

}
export default RandomNumber;

