class Data {
  
  TodayDate() {
    const now = new Date()     
    let day = (now.getDate())
    console.log(day)
    let month = (now.getMonth()+1)
    let year = now.getFullYear()
    if(day<10){
      day = "0" + now.getDate()      
    }
    if(month<10){
      month = "0" + now.getMonth()
    }  
    let date = day + "." + month + "." + year
    return date;    
  };
    
  YesDate() {  //Вчерашняя дата
    let yesterday = new Date()
    yesterday.setDate(yesterday.getDate() - 1);
    const now = new Date(yesterday)   

    let day = (now.getDate())
    
    console.log(now)
    let month = (now.getMonth()+1)
    let year = now.getFullYear()
    if(day<10){
      day = "0" + now.getDate()      
    }
    if(month<10){
      month = "0" + now.getMonth()
    }  
    let date = day + "." + month + "." + year
    return date;    
  };
      
  TomDate() {  //Завтрашняя дата в формате дд.мм.гггг(01.01.2023)  
    let yesterday = new Date()
    yesterday.setDate(yesterday.getDate() + 1);
    const now = new Date(yesterday)   

    let day = (now.getDate())
    
    console.log(now)
    let month = (now.getMonth()+1)
    let year = now.getFullYear()
    if(day<10){
      day = "0" + now.getDate()      
    }
    if(month<10){
      month = "0" + now.getMonth()
    }  
    let date = day + "." + month + "." + year
    return date;   
  };

  OldDate(minus) {  //Любая прошлая дата 
    let yesterday = new Date()
    yesterday.setDate(yesterday.getDate() - minus);
    const now = new Date(yesterday)   

    let day = (now.getDate())
    
    console.log(now)
    let month = (now.getMonth()+1)
    let year = now.getFullYear()
    if(day<10){
      day = "0" + now.getDate()      
    }
    if(month<10){
      month = "0" + now.getMonth()
    }  
    let date = day + "." + month + "." + year
    return date;    
  };

  FutureDate(plus) {  //Любая будущая дата  
    let yesterday = new Date()
    yesterday.setDate(yesterday.getDate() + plus);
    const now = new Date(yesterday)   

    let day = (now.getDate())
    
    console.log(now)
    let month = (now.getMonth()+1)
    let year = now.getFullYear()
    if(day<10){
      day = "0" + now.getDate()      
    }
    if(month<10){
      month = "0" + now.getMonth()
    }  
    let date = day + "." + month + "." + year
    return date;    
  };  
}
export default Data 
