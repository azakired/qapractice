class Letters{

    randomLetter(length) {
      let randomLetter = "";
      var pattern = "abcdefghijklmnopqrstuvwxyz";
      for (var i = 0; i < length; i++) {
        randomLetter += pattern.charAt(Math.floor(Math.random() * pattern.length));
      }   
      return randomLetter;
    };  

    randomLetterUC(length) {
      let randomLetter = "";
      var pattern = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      for (var i = 0; i < length; i++) {
        randomLetter += pattern.charAt(Math.floor(Math.random() * pattern.length));
      }   
      return randomLetter;
    };  
  }

export default Letters