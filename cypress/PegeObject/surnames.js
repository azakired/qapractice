class Surnames {
  //Создание рандомного предложения
  surnames = ["Johnson", "Brown", "Walker", "Hall", "White", "Wilson", "Thompson", "Abramson", "Adamson","Adderiy",
  "Addington","Adrian","Albertson","Aldridge","Allford","Alsopp","Anderson","Babcock","Backer", "Chapman", "Charlson",
  "Chesterton","Daniels","Davidson","Day","Dean","Derrick","Dickinson","Dodson","Donaldson","Eddington","Edwards",
  "Ellington","Elmers","Faber","Fane","Farmer","Farrell","Ferguson","Finch","Fisher","Fitzgerald","Flannagan",
  "Flatcher","Galbraith","Gardner", "Garrison","Gate","Gerald","Gibbs","Gilbert","Haig","Hailey","Hamphrey","Hancock",
  "Hardman","Harrison","Hawkins","Jacobson", "James","Jeff","Jenkin","Laird","Lamberts","Larkins","Lawman","Leapman",
  "MacAdam","MacAlister","MacDonald","Macduff","Macey","Mackenzie","Mansfield","Marlow","Marshman","Mason"];
  getRandomSurnames(firstLetterToUppercase = false) {
    const surname =
      this.surnames[this.randomNumber(0, this.surnames.length - 1)];
    return firstLetterToUppercase ? surname.charAt(0).toUpperCase() + surname.slice(1) : surname;
  }

  generateSurnames(length = 7) {
    let arr = [];
    for (let i = 0; i < length; i++) {arr.push(this.getRandomSurnames(i === 0));
    }
    return arr.join(" ").trim();
  }

  randomNumber(min, max) {
    return Math.round(Math.random() * (max - min) + min);
  }
}

export default Surnames;
