import Auth from '../../../PegeObject/autorization';
import Words from '../../../PegeObject/words';
import Letters from '../../../PegeObject/letters';
import RandomNumber from '../../../PegeObject/randomNumber';

describe('inputs', () => {
  const auth = new Auth();
  const words = new Words();
  const letters = new Letters();
  const randNum = new RandomNumber();

  let textStringEN = words.getRandomWordEN(2);
  let letterNumber = letters.randomLetter(1) + randNum.randomNumb(1);
  let letter25 = letters.randomLetter(25);
  let email = letters.randomLetter(3) + '@mail.com';
  let pass = letters.randomLetter(3) + letters.randomLetterUC(3) + randNum.randomNumb(1) + '$';

  it('Inputs', () => {
    auth.auth();
    cy.contains('Inputs').click();
    cy.get('.tabs').contains('Text input').parent().should('have.class', 'tab active').should('be.visible');
    //Отправка пустой формы
    cy.get('[placeholder="Submit me"]').as('SubmitMe').type('{enter}');
    //Проверка валидационного сообщения браузера
    cy.get('@SubmitMe').invoke('prop', 'validationMessage').should('equal', 'Please fill out this field.');
    //Проврка отображения не валидного ввода
    cy.get('.invalid-feedback').contains('This field is required.');
    //Ввод кирилицы
    cy.get('@SubmitMe').type(words.getRandomWordRU(2)).type('{enter}');
    cy.get('.invalid-feedback').contains('Enter a valid string consisting of letters, numbers, underscores or hyphens.');
    //Ввод латиницы
    cy.get('@SubmitMe').clear().type(textStringEN).should('have.value', textStringEN).type('{enter}');
    cy.get('#result-text').contains(textStringEN).should('be.visible');
    //Ввод одной буквы латиница
    cy.get('@SubmitMe').clear().type(letters.randomLetter(1)).type('{enter}');
    cy.get('.invalid-feedback').contains('Please enter 2 or more characters');
    //Ввод одной буквы латиница и одна цифра
    cy.get('@SubmitMe').clear().type(letterNumber).should('have.value', letterNumber).type('{enter}');
    cy.get('#result-text').contains(letterNumber).should('be.visible');
    //Ввод 25 букв латиница
    cy.get('@SubmitMe').clear().type(letter25).type('{enter}');
    cy.get('#result-text').contains(letter25).should('be.visible');
    //Ввод 26 букв латиница
    cy.get('@SubmitMe').clear().type(letters.randomLetter(26)).type('{enter}');
    cy.get('.invalid-feedback').contains('Please enter no more than 25 characters');
    //Ввод спец символов
    cy.get('@SubmitMe').clear().type('№;%:?*').type('{enter}');
    cy.get('.invalid-feedback').contains('Enter a valid string consisting of letters, numbers, underscores or hyphens.');

    //Email
    cy.contains('Email field').click();
    cy.get('.tabs').contains('Email').parent().should('have.class', 'tab active').should('be.visible');
    cy.get('[placeholder="Submit me"]').as('SubmitMe').type('{enter}');
    cy.get('@SubmitMe').invoke('prop', 'validationMessage').should('equal', 'Please fill out this field.');
    cy.get('.invalid-feedback').contains('This field is required.');
    cy.get('@SubmitMe').clear().type(letters.randomLetter(1)).type('{enter}');
    cy.get('.invalid-feedback').contains('Enter a valid email address.');
    cy.get('@SubmitMe').clear().type(email).should('have.value', email).type('{enter}');
    cy.get('#result-text').contains(email).should('be.visible');

    //Pass
    cy.contains('Password field').click();
    cy.get('.tabs').contains('Password field').parent().should('have.class', 'tab active').should('be.visible');
    cy.get('[placeholder="Submit me"]').as('SubmitMe').type('{enter}');
    cy.get('@SubmitMe').invoke('prop', 'validationMessage').should('equal', 'Please fill out this field.');
    cy.get('.invalid-feedback').contains('This field is required.');
    cy.get('@SubmitMe').clear().type(letters.randomLetter(1)).type('{enter}');
    cy.get('.invalid-feedback').contains('Low password complexity');
    cy.get('@SubmitMe').clear().type(letters.randomLetter(7)).type('{enter}');
    cy.get('.invalid-feedback').contains('Low password complexity');
    cy.get('@SubmitMe').clear().type(letters.randomLetter(4) + letters.randomLetterUC(4)).type('{enter}');
    cy.get('.invalid-feedback').contains('Low password complexity');
    cy.get('@SubmitMe').clear().type(letters.randomLetter(4) + 'A' + randNum.randomNumb(3)).type('{enter}');
    cy.get('.invalid-feedback').contains('Low password complexity');
    cy.get('@SubmitMe').clear().type(pass).should('have.value', pass).type('{enter}');
    cy.get('#result-text').contains(pass).should('be.visible');
  });
});
